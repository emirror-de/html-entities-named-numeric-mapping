<?php

declare(strict_types=1);

namespace W3C;

class HTMLEntities
{

    public static function getNumeric(string $name, string $version = '4.01')
    {
        switch ($version) {
        case '4.01':
            return strtr($name, HTMLEntities\Version401::getTranslationTable());
        default:
            return '';
            break;
        }
    }
}
