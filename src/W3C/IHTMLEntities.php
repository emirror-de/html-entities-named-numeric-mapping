<?php

declare(strict_types=1);

namespace W3C;

interface IHTMLEntities
{

    static public function getTranslationTable() : array;
}
