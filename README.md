# html-entities-named-numeric-mapping

Simple class that is able to translate the named HTML entities into the numeric ones.

Especially useful for storing json data in html data attributes, because the conversion to its characters is easy using JavaScript.

Version 4.01 has been copied from [this answer on StackOverflow](https://stackoverflow.com/a/11179875/11761856). Thanks to [hakre](https://stackoverflow.com/users/367456/hakre) for sharing his code.

# Usage

## php

```php
<?php

use W3C\HTMLEntities;

$named_entities = htmlentities("<>");
$numeric_entities = preg_replace_callback(
    "/(&[a-z]+;)/",
    function($m) {
        return HTMLEntities::getNumeric($m[0]);
        },
        $named_entities
        );
```

## JavaScript

```javascript
htmlEntitiesDecode = function(str) {
    return String(str).replace(/\&#([0-9]){1,4};/gim, (i) => {
        return String.fromCharCode(i.substr(2, (i.length - 3)));
    });
}
```
